# DM

Proyecto práctico de la asignatura DM

# Descripción de la aplicación

Se trata de una aplicación de gestión de tareas donde se visualizarán de forma listada las tareas siguiendo el formato descrito en otro punto. La aplicación
permitirá la categorización de las tareas asi como la asignación de prioridades de forma interactiva siguiendo el modelo de Getting Things Done.

# Funcionalidades 
https://cursos.faitic.uvigo.es/tema1920/claroline/document/goto/index.php/B%E1sico/Practica1920.pdf?cidReq=O06G150V01964

# Esquema de la base de datos

Las tablas de la BD tendrán los siguientes atributos:

|  Tarea | Categoría | Prioridad |
| ------ |  ------ |  ------ |
| nombre | nombre | nombre |
| descripción | abreviatura | color |
| estado | descripción |
| tiempo dedicado |
| tiempo estimado |
| fecha límite |
| idCategoria |
| idPrioridad |

# Menú principal

| Menú |
|----- |
| Añadir Tarea |
| Añadir Categoría |
| Ver Todas las Tareas |
| Ver Tareas Activas |
| Ver Tareas Completadas |
| Filtrar Categoría |
| Filtrar Prioridad |
| Borrar Múltiples Tareas |


# Menú contextual

| Menú|
|-----|
| Borrar Tarea |
| Cambiar Prioridad |
| Cambiar Categoría |
| Cambiar estado|
| Editar Tarea |
| Añadir tiempo |

# Formato de Vista de tarea
|  Nombre de tarea | Tiempo restante | Estado | Abreviatura de categoría |
| ------ | ------ | ------ | ------ |

