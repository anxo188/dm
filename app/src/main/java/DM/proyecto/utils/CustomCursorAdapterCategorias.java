package DM.proyecto.utils;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import DM.proyecto.DBManager.DBManager;
import DM.proyecto.R;

public class CustomCursorAdapterCategorias extends CursorAdapter {



    public CustomCursorAdapterCategorias(Context context, Cursor c) {
            super(context,c,false);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {//item context
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View inflate = layoutInflater.inflate(R.layout.item_categoria, parent, false);

        return inflate;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        DBManager db = new DBManager( context);

        TextView nombre = view.findViewById(R.id.catNombre);
            nombre.setText(
                    cursor.getString(
                            cursor.getColumnIndex(DBManager.CATEGORIA_COL_NOMBRE)
                    )
            );
        TextView abrev = view.findViewById(R.id.catAbrv);
            abrev.setText(
                    cursor.getString(
                            cursor.getColumnIndex(DBManager.CATEGORIA_COL_ABREVIATURA)
                    )
            );
    }
}

