package DM.proyecto.utils;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;

import DM.proyecto.DBManager.DBManager;
import DM.proyecto.R;

public class CustomCursorAdapterTareas extends CursorAdapter {

    public CustomCursorAdapterTareas(Context context, Cursor c) {
        super(context,c,false);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {//item context
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View inflate = layoutInflater.inflate(R.layout.item_tarea, parent, false);

        return inflate;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        DBManager db = new DBManager( context);

       String nombre = cursor.getString(
               cursor.getColumnIndex(DBManager.TAREA_COL_NOMBRE)
       );
       TextView nombreTarea = view.findViewById(R.id.nombreTarea);
       nombreTarea.setText(nombre);

        String fechaLimite = cursor.getString(
                cursor.getColumnIndex(DBManager.TAREA_COL_FECHALIMITE)
        );

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        TextView restante = view.findViewById(R.id.tiempoRestante);
        if(fechaLimite != null){
            try{
                restante.setText( calcularDateDiff(sdf.parse(fechaLimite)) );
            }catch(ParseException e){
                e.printStackTrace();
            }
        }

        Cursor prioridadFila = db.getPrio(cursor.getInt(
                cursor.getColumnIndex(DBManager.TAREA_COL_PRIORIDADID)
        ));
        LinearLayout fila = view.findViewById(R.id.listadoTareas);
        if( prioridadFila.moveToFirst() ){
            String color = prioridadFila.getString(prioridadFila.getColumnIndex(DBManager.PRIORIDAD_COL_COLOR));
            try{
                fila.setBackgroundColor(context.getResources().getColor(Integer.parseInt(color),null));
            }catch(Exception e){
                Log.d("Asignar Color: ",color);
                e.printStackTrace();
            }
        }else{
            fila.setBackgroundColor(context.getResources().getColor(R.color.white,null));
        }

        int estado = cursor.getInt(
                cursor.getColumnIndex(DBManager.TAREA_COL_ESTADO)
        );
        TextView estadoTxt = view.findViewById(R.id.estado);
        switch(estado){//1: Sin empezar 2: En curso 3: Pausada 4: Finalizada
            case 1:
                estadoTxt.setText(context.getResources().getString(R.string.sinEmpezar));
                break;
            case 2:
                estadoTxt.setText(context.getResources().getString(R.string.enCurso));
                break;
            case 3:
                estadoTxt.setText(context.getResources().getString(R.string.pausada));
                break;
            case 4:
                estadoTxt.setText(context.getResources().getString(R.string.finalizada));
                break;
        }

        Cursor categoria = db.getCatAbrv(cursor.getInt(
                cursor.getColumnIndex(DBManager.TAREA_COL_CATEGORIAID)
        ));
        TextView cat = view.findViewById(R.id.abreviaturaCat);
        if(categoria.moveToFirst()){
            String abv = categoria.getString(categoria.getColumnIndex(DBManager.CATEGORIA_COL_ABREVIATURA));
            cat.setText(abv);
        }else{
            cat.setText(context.getResources().getString(R.string.sinCat));
        }
//TODO
        //fila.setOnClickListener();
    }

    private String calcularDateDiff(Date limite){
        Calendar calLimite = Calendar.getInstance();

        calLimite.setTime( limite );

        long minutes = ChronoUnit.MINUTES.between( Calendar.getInstance().toInstant(), calLimite.toInstant() );
        long hours = minutes / 60;
        minutes = minutes % 60;
        long days = hours / 24;
        hours = hours % 24;



        String result = Long.toString(days)+"d "+Long.toString(hours)+"h:"+Long.toString(minutes)+"m";
        return result;
    }


}
