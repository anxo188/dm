package DM.proyecto.SecondaryActivities;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.regex.Pattern;

import DM.proyecto.DBManager.DBManager;
import DM.proyecto.R;


public class EditarTarea extends Activity {//Para devolver control a Main this.finish();
    int idTarea;
    DBManager gestorDB;
    String nombre;
    String descripcion;
    int estado;
    int catId;
    int prioId;
    int tiempoDedicado;
    int tiempoEstimado;
    String fechaLimite;

    Cursor tareaCursor;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        this.setContentView( R.layout.modificar_tarea );
        gestorDB = new DBManager( this.getApplicationContext());
        Intent intent = this.getIntent();
        idTarea = (Integer) intent.getExtras().get("idTarea") ;

        Button cancelar = this.findViewById(R.id.editCancelar);
        Button confirmar = this.findViewById(R.id.editConfirmar);
        if(idTarea == -1){
            prepareSpinnerCategories(1);
            prepareSpinnerEstados(1);
            EditText fLimit = this.findViewById(R.id.fLimTareaEdit);
            fLimit.setText("yyyy-MM-dd HH:MM:SS");
            confirmar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    actualizarDatos();
                    if(check()) {
                        try {
                            gestorDB.insertarTarea(
                                    nombre,
                                    descripcion,
                                    estado,
                                    catId,
                                    prioId,
                                    tiempoDedicado,
                                    tiempoEstimado,
                                    fechaLimite
                            );
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        finish();
                    }
                }
            });
        }
        else {
            tareaCursor = gestorDB.getTarea(idTarea);
            if(tareaCursor.moveToFirst()){
                inicializarDatos();
                confirmar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        actualizarDatos();
                        if(check()) {
                            try {
                                gestorDB.modificarTarea(
                                        idTarea,
                                        nombre,
                                        descripcion,
                                        estado,
                                        catId,
                                        prioId,
                                        tiempoDedicado,
                                        tiempoEstimado,
                                        fechaLimite
                                );

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            finish();
                        }
                    }
                });
                fillView();
            }
        }


        cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }


    private void actualizarDatos(){
        EditText nombre = this.findViewById(R.id.nombreTareaEdit);
        this.nombre = nombre.getText().toString();

        EditText descripcion = this.findViewById(R.id.descripcionTareaEdit);
        this.descripcion = descripcion.getText().toString();

        EditText tEstimado = this.findViewById(R.id.tEstimadoTareaEdit);
        this.tiempoEstimado = Integer.parseInt( tEstimado.getText().toString() );

        EditText tDedicado = this.findViewById(R.id.tDedicadoTareaEdit);
        this.tiempoDedicado = Integer.parseInt( tDedicado.getText().toString());

        EditText fLimit = this.findViewById(R.id.fLimTareaEdit);
        this.fechaLimite = fLimit.getText().toString();
    }
    private void fillView(){

            EditText nombre = this.findViewById(R.id.nombreTareaEdit);
            nombre.setText(this.nombre);

            EditText descripcion = this.findViewById(R.id.descripcionTareaEdit);
            descripcion.setText(this.descripcion);

            EditText tEstimado = this.findViewById(R.id.tEstimadoTareaEdit);
            tEstimado.setText(Integer.toString(this.tiempoEstimado));

            EditText tDedicado = this.findViewById(R.id.tDedicadoTareaEdit);
            tDedicado.setText(Integer.toString(this.tiempoDedicado));

            EditText fLimit = this.findViewById(R.id.fLimTareaEdit);
            fLimit.setText(this.fechaLimite);


            prepareSpinnerEstados(
                    tareaCursor.getInt(
                        tareaCursor.getColumnIndex(DBManager.TAREA_COL_ESTADO)
                    )
            );

            prepareSpinnerCategories(
                    tareaCursor.getInt(
                            tareaCursor.getColumnIndex(DBManager.TAREA_COL_CATEGORIAID)
                    )
            );



    }
    private void inicializarDatos(){
        nombre = tareaCursor.getString(
                tareaCursor.getColumnIndex(DBManager.TAREA_COL_NOMBRE)
        );
        descripcion= tareaCursor.getString(
                tareaCursor.getColumnIndex(DBManager.TAREA_COL_DESCRIPCION)
        );
        estado= tareaCursor.getInt(
                tareaCursor.getColumnIndex(DBManager.TAREA_COL_ESTADO)
        );
        catId= tareaCursor.getInt(
                tareaCursor.getColumnIndex(DBManager.TAREA_COL_CATEGORIAID)
        );
        prioId= tareaCursor.getInt(
                tareaCursor.getColumnIndex(DBManager.TAREA_COL_PRIORIDADID)
        );
        tiempoDedicado= tareaCursor.getInt(
                tareaCursor.getColumnIndex(DBManager.TAREA_COL_TIEMPODEDICADO)
        );
        tiempoEstimado= tareaCursor.getInt(
                tareaCursor.getColumnIndex(DBManager.TAREA_COL_TIEMPOESTIMADO)
        );
        fechaLimite= tareaCursor.getString(
                tareaCursor.getColumnIndex(DBManager.TAREA_COL_FECHALIMITE)
        );
    }

    private void prepareSpinnerEstados(int selected){

        Spinner estados = this.findViewById(R.id.estadoTareaEdit);
        ArrayAdapter<CharSequence> adapter =
                ArrayAdapter.createFromResource(this,
                        R.array.valores_estado,
                        android.R.layout.simple_spinner_item);
        estados.setAdapter(adapter);
        estados.setSelection(selected-1);

        estados.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                estado=position+1;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private boolean checkDateFormat(){
        if( !Pattern.matches("^[0-9]{4}\\-(0[1-9]|1[0-2])\\-(0[1-9]|[1-2][0-9]|3[0-1])\\s[0-9]{2}\\:[0-9]{2}\\:[0-9]{2}$",this.fechaLimite)){
            return false;
        }
        return true;
    }

    private boolean check(){
        if(this.nombre.isEmpty()){
            String msg = "Deben completarse los datos: Nombre ";
            Toast.makeText( this, msg, Toast.LENGTH_LONG ).show();
            return false;
        }else if(this.checkDateFormat() == false){
            String msg = "Formato de fecha limite incorrecto ";
            Toast.makeText( this, msg, Toast.LENGTH_LONG ).show();
            return false;
        }else if(this.fechaLimite.isEmpty()){
            String msg = "Deben completarse los datos: Fecha Limite ";
            Toast.makeText( this, msg, Toast.LENGTH_LONG ).show();
            return false;
        }
        return true;
    }

    private void prepareSpinnerCategories(int selected){
        Spinner categories = this.findViewById(R.id.categoriaTareaEdit);
        final Cursor categorias = gestorDB.getCategorias();
        final String[] categoriasNombre = new String[categorias.getCount()];
        int index = 0;
        while(categorias.moveToNext()){
            categoriasNombre[index] = categorias.getString(
                    categorias.getColumnIndex(DBManager.CATEGORIA_COL_NOMBRE)
            );
            index++;
        }

        ArrayAdapter<String> adapter =
                new ArrayAdapter<String>(this,
                        android.R.layout.simple_spinner_item,
                        categoriasNombre);
        categories.setAdapter(adapter);
        categories.setSelection(selected-1);

        categories.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                catId = gestorDB.getCatId(categoriasNombre[position]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
}

