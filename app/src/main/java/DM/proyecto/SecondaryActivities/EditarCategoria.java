package DM.proyecto.SecondaryActivities;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import DM.proyecto.DBManager.DBManager;
import DM.proyecto.R;

public class EditarCategoria extends Activity {
    int idCat;
    DBManager gestorDB;
    String abrev;
    String descripcion;
    String nombre;
    Cursor catCursor;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.modificar_categoria);
        gestorDB = new DBManager( this.getApplicationContext());
        Intent intent = this.getIntent();
        idCat = (Integer) intent.getExtras().get("idCat") ;

        Button cancelar = this.findViewById(R.id.editCancelar);
        Button confirmar = this.findViewById(R.id.editConfirmar);

        if(idCat == -1){
            confirmar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    actualizarDatos();
                    if(check()){
                        try{
                            gestorDB.insertarCategoria(
                                    nombre,
                                    abrev,
                                    descripcion
                            );
                        }catch(Exception e){
                            e.printStackTrace();
                        }
                        finish();
                    }
                }
            });
        }
        else {
            catCursor = gestorDB.getCategoria(idCat);
            if(catCursor.moveToFirst()){
                inicializarDatos();
                confirmar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        actualizarDatos();
                        if(check()) {
                            try {
                                gestorDB.modificarCategoria(
                                        idCat,
                                        nombre,
                                        abrev,
                                        descripcion
                                );

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            finish();
                        }
                    }
                });
                fillView();
            }
        }


        cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void actualizarDatos(){
        EditText nombre = this.findViewById(R.id.editNombreCat);
        this.nombre = nombre.getText().toString();

        EditText abreviatura = this.findViewById(R.id.abrevInput);
        this.abrev = abreviatura.getText().toString();

        EditText descripcion = this.findViewById(R.id.catDescriptionEdit);
        this.descripcion = descripcion.getText().toString();
    }
    private boolean check(){
        if(this.abrev.isEmpty()){
            String msg = "Deben completarse los datos: Abreviatura";
            Toast.makeText( this, msg, Toast.LENGTH_LONG ).show();
            return false;
        }else if(this.nombre.isEmpty()){
            String msg = "Deben completarse los datos: Nombre ";
            Toast.makeText( this, msg, Toast.LENGTH_LONG ).show();
            return false;
        }
        return true;
    }

    private void fillView(){

        EditText nombre = this.findViewById(R.id.editNombreCat);
        nombre.setText(this.nombre);

        EditText abreviatura = this.findViewById(R.id.abrevInput);
        abreviatura.setText(this.abrev);

        EditText descripcion = this.findViewById(R.id.catDescriptionEdit);
        descripcion.setText(this.descripcion);

    }
    private void inicializarDatos(){
        nombre = catCursor.getString(
                catCursor.getColumnIndex(DBManager.CATEGORIA_COL_NOMBRE)
        );
        descripcion= catCursor.getString(
                catCursor.getColumnIndex(DBManager.CATEGORIA_COL_DESCRIPCION)
        );

    }
}
