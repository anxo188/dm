package DM.proyecto.SecondaryActivities;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import DM.proyecto.DBManager.DBManager;
import DM.proyecto.R;
import DM.proyecto.utils.CustomCursorAdapterCategorias;

public class ShowCats extends Activity {

    private DBManager gestorDB;
    private CustomCursorAdapterCategorias adaptadorDB;
    private ListView listado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        this.gestorDB = new DBManager( this.getApplicationContext());
        this.setContentView( R.layout.listado_categoria);
        ListView listado_categorias = this.findViewById( R.id.category_List );
        this.registerForContextMenu( listado_categorias );


    }

    @Override
    public void onStart(){
        super.onStart();

        listado = this.findViewById( R.id.category_List );
        this.adaptadorDB = new CustomCursorAdapterCategorias(this,null);
        listado.setAdapter(this.adaptadorDB);
        this.registerForContextMenu(listado);
        actualizarVista();

        Button atras = this.findViewById(R.id.buttonAtras);
        atras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void actualizarVista(){
        this.adaptadorDB.changeCursor(gestorDB.getCategorias());
        this.adaptadorDB.notifyDataSetChanged();
    }



    public void onCreateContextMenu(ContextMenu contxt, View v, ContextMenu.ContextMenuInfo cmi){

        this.getMenuInflater().inflate( R.menu.menu_contextual_categorias, contxt );
        contxt.setHeaderTitle( R.string.opciones );

    }

    @Override
    public boolean onContextItemSelected(MenuItem item){
        boolean aux = super.onContextItemSelected(item);
        int position = ( (AdapterView.AdapterContextMenuInfo) item.getMenuInfo()).position;
        Cursor cursor = this.adaptadorDB.getCursor();

        switch ( item.getItemId() ){
            case R.id.option_BorrarCategoria:
                if ( cursor.moveToPosition( position ) ) {
                    final int id = cursor.getInt( cursor.getColumnIndex(DBManager.CATEGORIA_COL_ID) );
                    this.gestorDB.eliminarCategoria( id );
                    this.actualizarVista();
                    Toast.makeText( this, "Categoria eliminada", Toast.LENGTH_LONG ).show();
                    aux = true;
                } else {
                    String msg = "No se pudo eliminar ";
                    Log.e( "context_eliminaCategoria", msg );
                    Toast.makeText( this, msg, Toast.LENGTH_LONG ).show();
                }
                break;
            case R.id.option_EditarCategoria:
                if(cursor.moveToPosition(position)){
                    final int id = cursor.getInt( cursor.getColumnIndex(DBManager.CATEGORIA_COL_ID) );
                    Intent myIntent = new Intent( this, EditarCategoria.class );
                    myIntent.putExtra("idCat",id);
                    this.startActivity(myIntent);
                    actualizarVista();
                }else{
                    String msg = "No se pudo modificar ";
                    Log.e( "context_modificarTarea", msg );
                    Toast.makeText( this, msg, Toast.LENGTH_LONG ).show();
                }

                break;
        }

        return aux;
    }
}
