package DM.proyecto.DBManager;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.regex.Pattern;
import DM.proyecto.R;

public class DBManager extends SQLiteOpenHelper{
    public static final String DB_NOMBRE = "TodoTasksDB";
    public static final int DB_VERSION = 33;

    public boolean creada = false;
    public static final String TABLA_TAREA = "tarea";
    public static final String TABLA_CATEGORIA = "categoria";
    public static final String TABLA_PRIORIDAD = "prioridad";

    public static final String TAREA_COL_ID = "_id";
    public static final String TAREA_COL_NOMBRE = "nombre";
    public static final String TAREA_COL_ESTADO = "estado";
    public static final String TAREA_COL_DESCRIPCION = "descripcion";
    public static final String TAREA_COL_TIEMPODEDICADO = "tiempoDedicado";
    public static final String TAREA_COL_TIEMPOESTIMADO = "tiempoEstimado";
    public static final String TAREA_COL_FECHALIMITE = "fechaLimite";
    public static final String TAREA_COL_CATEGORIAID = "idCategoria";
    public static final String TAREA_COL_PRIORIDADID = "idPrioridad";

    public static final String CATEGORIA_COL_ID= "_id";
    public static final String CATEGORIA_COL_NOMBRE= "nombre";
    public static final String CATEGORIA_COL_ABREVIATURA= "abreviatura";
    public static final String CATEGORIA_COL_DESCRIPCION= "descripcion";


    public static final String PRIORIDAD_COL_ID = "_id";
    public static final String PRIORIDAD_COL_NOMBRE = "nombre";
    public static final String PRIORIDAD_COL_COLOR = "color";


    public DBManager(Context context)
    {
        super( context, DB_NOMBRE, null, DB_VERSION);

    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        Log.i(  "DBManager",
                "Creando BD " + DB_NOMBRE + " v" + DB_VERSION);
        boolean cerrada =false;
        try {

            Log.i(  "DBManager",
                    "Creando Tabla: " + TABLA_PRIORIDAD);
            db.beginTransaction();
            db.execSQL( "CREATE TABLE IF NOT EXISTS " + TABLA_PRIORIDAD + "( "
                    + PRIORIDAD_COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT , "
                    + PRIORIDAD_COL_NOMBRE + " string(128) NOT NULL,"
                    + PRIORIDAD_COL_COLOR + " int NOT NULL)");
            db.setTransactionSuccessful();
            db.endTransaction();
            Log.i(  "DBManager",
                    "Tabla creada: " + TABLA_PRIORIDAD);

            Log.i(  "DBManager",
                    "Creando Tabla: " + TABLA_CATEGORIA);
            db.beginTransaction();
            db.execSQL( "CREATE TABLE IF NOT EXISTS " + TABLA_CATEGORIA + "( "
                    + CATEGORIA_COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + CATEGORIA_COL_NOMBRE + " string(128) NOT NULL,"
                    + CATEGORIA_COL_ABREVIATURA + " string(6) NOT NULL,"
                    + CATEGORIA_COL_DESCRIPCION + " string(256))");
            db.setTransactionSuccessful();
            db.endTransaction();
            Log.i(  "DBManager",
                    "Tabla creada: " + TABLA_CATEGORIA);

            Log.i(  "DBManager",
                    "Creando Tabla: " + TABLA_TAREA);
            db.beginTransaction();
            db.execSQL( "CREATE TABLE IF NOT EXISTS " + TABLA_TAREA + "( "
                    + TAREA_COL_ID + " INTEGER PRIMARY KEY  AUTOINCREMENT, "
                    + TAREA_COL_NOMBRE + " string(128) NOT NULL,"
                    + TAREA_COL_ESTADO + " int NOT NULL,"//1: Sin empezar 2: En curso 3: Pausada 4: Finalizada
                    + TAREA_COL_DESCRIPCION + " string(256),"
                    + TAREA_COL_CATEGORIAID + " int DEFAULT NULL,"
                    + TAREA_COL_PRIORIDADID + " int DEFAULT NULL,"
                    + TAREA_COL_TIEMPODEDICADO + " int , "
                    + TAREA_COL_TIEMPOESTIMADO + " int ,"
                    + TAREA_COL_FECHALIMITE + " DATE,"
                    + "FOREIGN KEY("+ TAREA_COL_PRIORIDADID+") REFERENCES "
                    + TABLA_PRIORIDAD +"("+PRIORIDAD_COL_ID+") ON DELETE SET NULL,"
                    + "FOREIGN KEY("+ TAREA_COL_CATEGORIAID+") REFERENCES "
                    + TABLA_CATEGORIA +"("+CATEGORIA_COL_ID +") ON DELETE SET NULL )");//Formato YYYY-MM-DD HH:MM:SS
            db.setTransactionSuccessful();
            db.endTransaction();
            cerrada = true;

            Log.i(  "DBManager",
                    "Tabla creada: " + TABLA_TAREA);
            Log.i(  "DBManager",
                    "Creando prioridades ... ");

            this.insertarPrioridad(db,"Alta", R.color.pAlta);//Rojo
            this.insertarPrioridad(db,"Media",R.color.pMedia);//Verde
            this.insertarPrioridad(db,"Baja",R.color.pBaja);//Azul
            this.insertarPrioridad(db,"Ninguna",R.color.white);//Blanco
            Log.i(  "DBManager",
                    "Cargando datos de ejemplo ... ");
            this.loadExampleData(db);
        }
        catch(SQLException exc)
        {
            Log.e( "DBManager.onCreate", exc.getMessage() );
        }catch(Exception exc){
            Log.e( "DBManager.onCreate", exc.getMessage() );
        }
        finally {
            if(!cerrada)
                db.endTransaction();

        }


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        Log.i(  "DBManager",
                "DB: " + DB_NOMBRE + ": v" + oldVersion + " -> v" + newVersion );
        boolean cerrada = false;
        try {
            db.beginTransaction();
            db.execSQL( "DROP TABLE IF EXISTS " + TABLA_TAREA );
            db.setTransactionSuccessful();
            db.endTransaction();
            db.beginTransaction();
            db.execSQL( "DROP TABLE IF EXISTS " + TABLA_CATEGORIA );
            db.setTransactionSuccessful();
            db.endTransaction();
            db.beginTransaction();
            db.execSQL( "DROP TABLE IF EXISTS " + TABLA_PRIORIDAD );
            db.setTransactionSuccessful();
            db.endTransaction();
            cerrada = true;
        }  catch(SQLException exc) {
            Log.e( "DBManager.onUpgrade", exc.getMessage() );
        }
        finally {
            if(!cerrada)
                db.endTransaction();
        }

        this.onCreate( db );
    }

    public void clearDB(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLA_TAREA,null,null);
        db.delete(TABLA_CATEGORIA,null,null);
        db.delete(TABLA_PRIORIDAD,null,null);
        loadExampleData();
    }
    /** Devuelve todas las tareas en la BD
     * @return Un Cursor con las tareas. */
    public Cursor getTareas()
    {
        return this.getReadableDatabase().query( TABLA_TAREA,
                null, null, null, null, null, null );

    }
    public Cursor getTarea(int idTarea)
    {
        return this.getReadableDatabase().query( TABLA_TAREA,
                null, TAREA_COL_ID +"= ?", new String[]{Integer.toString(idTarea)}, null, null, null );

    }

    /** Devuelve todas las categorias en la BD
     * @return Un Cursor con las categorias. */
    public Cursor getCategorias()
    {
        return this.getReadableDatabase().query( TABLA_CATEGORIA,
                null, null, null, null, null, null );
    }
    public Cursor getCategoria(int idCat)
    {
        return this.getReadableDatabase().query( TABLA_CATEGORIA,
                null, CATEGORIA_COL_ID +"= ?", new String[]{Integer.toString(idCat)}, null, null, null );

    }


    /** Devuelve todas las prioridades en la BD
     * @return Un Cursor con las prioridades. */
    public Cursor getPrioridades()
    {
        return this.getReadableDatabase().query( TABLA_PRIORIDAD,
                null, null, null, null, null, null );
    }

    /** Inserta una nueva tarea.
     * @param nombre El nombre de la tarea.
     * @param descripcion La descripcion de la tarea.
     * @param estado Estado de la tarea.
     * @param tiempoDedicado Tiempo dedicado a la tarea.
     * @param tiempoEstimado Tiempo estimado de la tarea.
     * @param fechaLimite Fecha limite de realizacion de la tarea.
     * @return true si se pudo insertar (o modificar), false en otro caso.
     */
    public boolean insertarTarea(String nombre, String descripcion, int estado,int idCat,int idPrio, int tiempoDedicado, int tiempoEstimado, String fechaLimite) throws Exception
    {
        return insertarTarea( null,  nombre,  descripcion,  estado,idCat,idPrio,  tiempoDedicado,  tiempoEstimado, fechaLimite);
    }

    public boolean insertarTarea(SQLiteDatabase db, String nombre, String descripcion, int estado,int idCat,int idPrio, int tiempoDedicado, int tiempoEstimado, String fechaLimite) throws Exception
    {
        Cursor cursor = null;
        boolean toret = false;
        if (db == null)
             db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        String uuid;
        Boolean correcto =false;
        if( !Pattern.matches("^[0-9]{4}\\-(0[1-9]|1[0-2])\\-(0[1-9]|[1-2][0-9]|3[0-1])\\s[0-9]{2}\\:[0-9]{2}\\:[0-9]{2}$",fechaLimite)){
            throw new Exception("Formato de fecha incorrecto, Recomendado 'YYYY-MM-DD HH:MM:SS'\n introducido: "+fechaLimite);
        }

        values.put( TAREA_COL_NOMBRE, nombre );
        values.put( TAREA_COL_DESCRIPCION, descripcion );
        values.put( TAREA_COL_ESTADO, estado );
        values.put(TAREA_COL_PRIORIDADID,idPrio);
        values.put(TAREA_COL_CATEGORIAID,idCat);
        values.put( TAREA_COL_TIEMPODEDICADO, tiempoDedicado );
        values.put( TAREA_COL_TIEMPOESTIMADO, tiempoEstimado );
        values.put( TAREA_COL_FECHALIMITE, fechaLimite );

        try {
            db.beginTransaction();
            db.insert( TABLA_TAREA, null, values );
            db.setTransactionSuccessful();
            toret = true;
        } catch(SQLException exc)
        {
            Log.e( "DBManager.inserta", exc.getMessage() );
        }
        finally {
            if ( cursor != null ) {
                cursor.close();
            }
            db.endTransaction();
        }

        return toret;
    }


    /** Inserta una nueva categoria.
     * @param nombre El nombre de la categoria.
     * @param descripcion La descripcion de la categoria.
     * @param abreviatura Abreviatura de la categoria.
     * @return true si se pudo insertar, false en otro caso.
     */
    public boolean insertarCategoria(String nombre, String abreviatura, String descripcion) throws Exception
    {
        return insertarCategoria(null,nombre , abreviatura,descripcion);
    }
    public boolean insertarCategoria(SQLiteDatabase db,String nombre, String abreviatura, String descripcion) throws Exception
    {
        Cursor cursor = null;
        boolean toret = false;
        if(db == null)
            db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        String uuid;
        Boolean correcto =false;

        values.put( CATEGORIA_COL_NOMBRE, nombre );
        values.put( CATEGORIA_COL_DESCRIPCION, descripcion );
        values.put( CATEGORIA_COL_ABREVIATURA, abreviatura );

        try {
            db.beginTransaction();
            db.insert( TABLA_CATEGORIA, null, values );
            db.setTransactionSuccessful();
            toret = true;
        } catch(SQLException exc)
        {
            Log.e( "DBManager.inserta", exc.getMessage() );
        }
        finally {
            if ( cursor != null ) {
                cursor.close();
            }
            db.endTransaction();
        }

        return toret;
    }

    /** Inserta una nueva prioridad.
     * @param nombre El nombre de la prioridad.
     * @param color El color de la prioridad.
     * @return true si se pudo insertar, false en otro caso.
     */
    public boolean insertarPrioridad(String nombre, int color) throws Exception{
        return insertarPrioridad(null,nombre,color);
    }
    public boolean insertarPrioridad(SQLiteDatabase db,String nombre, int color) throws Exception
    {
        Cursor cursor = null;
        boolean toret = false;
        if(db == null)
            db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        String uuid;
        Boolean correcto =false;

        values.put( PRIORIDAD_COL_NOMBRE, nombre );
        values.put( PRIORIDAD_COL_COLOR, color );


        try {
            db.beginTransaction();
            db.insert( TABLA_PRIORIDAD, null, values );
            db.setTransactionSuccessful();
            toret = true;
        } catch(SQLException exc)
        {
            Log.e( "DBManager.inserta", exc.getMessage() );
        }
        finally {
            if ( cursor != null ) {
                cursor.close();
            }
            db.endTransaction();
        }

        return toret;
    }

    /** Modifica una tarea existente.
     * @param uuid El identificador de la tarea.
     * @param nombre El nombre de la tarea.
     * @param descripcion La descripcion de la tarea.
     * @param estado Estado de la tarea.
     * @param tiempoDedicado Tiempo dedicado a la tarea.
     * @param tiempoEstimado Tiempo estimado de la tarea.
     * @param fechaLimite Fecha limite de realizacion de la tarea.
     * @return true si se pudo modificar, false en otro caso.
     */
    public boolean modificarTarea(int uuid, String nombre, String descripcion, int estado, int catId,int prioId, int tiempoDedicado, int tiempoEstimado, String fechaLimite) throws Exception
    {
        Cursor cursor = null;
        boolean toret = false;
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        if( !Pattern.matches("^[0-9]{4}\\-(0[1-9]|1[0-2])\\-(0[1-9]|[1-2][0-9]|3[0-1])\\s[0-9]{2}\\:[0-9]{2}\\:[0-9]{2}$",fechaLimite.toString())){
            throw new Exception("Formato de fecha incorrecto, Recomendado 'YYYY-MM-DD HH:MM:SS'");
        }
        values.put( TAREA_COL_ID, uuid);
        values.put( TAREA_COL_NOMBRE, nombre );
        values.put( TAREA_COL_DESCRIPCION, descripcion );
        values.put( TAREA_COL_ESTADO, estado );
        values.put(TAREA_COL_CATEGORIAID,catId);
        values.put(TAREA_COL_PRIORIDADID,prioId);
        values.put( TAREA_COL_TIEMPODEDICADO, tiempoDedicado );
        values.put( TAREA_COL_TIEMPOESTIMADO, tiempoEstimado );
        values.put( TAREA_COL_FECHALIMITE, fechaLimite );

        try {
            db.beginTransaction();
            int aux = db.update(TABLA_TAREA, values, TAREA_COL_ID + "= ?", new String[]{Integer.toString(uuid)});
            Log.e("DBManager.modificarTarea","Modificadas: "+aux+" filas");
            db.setTransactionSuccessful();
            toret = true;
        } catch(SQLException exc)
        {
            Log.e( "DBManager.modificarTarea", exc.getMessage() );
        }
        finally {
            if ( cursor != null ) {
                cursor.close();
            }
            db.endTransaction();
        }

        return toret;

    }

    public void asignarPrioridad(int idTarea, int idPrio){
        Cursor tarea = this.getTarea(idTarea);
        if(tarea.moveToFirst()){
            String nombre = tarea.getString(tarea.getColumnIndex(TAREA_COL_NOMBRE));
            String descripcion = tarea.getString(tarea.getColumnIndex(TAREA_COL_DESCRIPCION));
            int catId = tarea.getInt(tarea.getColumnIndex(TAREA_COL_CATEGORIAID));
            int estado = tarea.getInt(tarea.getColumnIndex(TAREA_COL_ESTADO));
            int tiempoDedicado = tarea.getInt(tarea.getColumnIndex(TAREA_COL_TIEMPODEDICADO));
            int tiempoEstimado = tarea.getInt(tarea.getColumnIndex(TAREA_COL_TIEMPOESTIMADO));
            String fechaLimite = tarea.getString(tarea.getColumnIndex(TAREA_COL_FECHALIMITE));
            try {
                this.modificarTarea(idTarea, nombre, descripcion, estado, catId, idPrio, tiempoDedicado, tiempoEstimado, fechaLimite);
            }catch(Exception e){
                e.printStackTrace();
            }
        }

    }

    public void asignarCat(int idTarea, int catId){

        Cursor tarea = this.getTarea(idTarea);
        if(tarea.moveToFirst()){
            String nombre = tarea.getString(tarea.getColumnIndex(TAREA_COL_NOMBRE));
            String descripcion = tarea.getString(tarea.getColumnIndex(TAREA_COL_DESCRIPCION));
            int idPrio = tarea.getInt(tarea.getColumnIndex(TAREA_COL_PRIORIDADID));
            int estado = tarea.getInt(tarea.getColumnIndex(TAREA_COL_ESTADO));
            int tiempoDedicado = tarea.getInt(tarea.getColumnIndex(TAREA_COL_TIEMPODEDICADO));
            int tiempoEstimado = tarea.getInt(tarea.getColumnIndex(TAREA_COL_TIEMPOESTIMADO));
            String fechaLimite = tarea.getString(tarea.getColumnIndex(TAREA_COL_FECHALIMITE));
            try {

                this.modificarTarea(idTarea, nombre, descripcion, estado, catId, idPrio, tiempoDedicado, tiempoEstimado, fechaLimite);
            }catch(Exception e){
                e.printStackTrace();
            }
        }
    }
    public void sumarTiempo(int idTarea,int tiempo){
        Cursor tarea = this.getTarea(idTarea);
        if(tarea.moveToFirst()){
            String nombre = tarea.getString(tarea.getColumnIndex(TAREA_COL_NOMBRE));
            String descripcion = tarea.getString(tarea.getColumnIndex(TAREA_COL_DESCRIPCION));
            int estado =tarea.getInt(tarea.getColumnIndex(TAREA_COL_ESTADO));
            int idPrio = tarea.getInt(tarea.getColumnIndex(TAREA_COL_PRIORIDADID));
            int catId = tarea.getInt(tarea.getColumnIndex(TAREA_COL_CATEGORIAID));
            int tiempoDedicado = tarea.getInt(tarea.getColumnIndex(TAREA_COL_TIEMPODEDICADO));
            int tiempoEstimado = tarea.getInt(tarea.getColumnIndex(TAREA_COL_TIEMPOESTIMADO));
            String fechaLimite = tarea.getString(tarea.getColumnIndex(TAREA_COL_FECHALIMITE));
            try {
                this.modificarTarea(idTarea, nombre, descripcion, estado, catId, idPrio, tiempoDedicado+tiempo, tiempoEstimado, fechaLimite);
            }catch(Exception e){
                e.printStackTrace();
            }
        }
    }
    public void asignarStat(int idTarea, int status){
        Cursor tarea = this.getTarea(idTarea);
        if(tarea.moveToFirst()){
            String nombre = tarea.getString(tarea.getColumnIndex(TAREA_COL_NOMBRE));
            String descripcion = tarea.getString(tarea.getColumnIndex(TAREA_COL_DESCRIPCION));
            int idPrio = tarea.getInt(tarea.getColumnIndex(TAREA_COL_PRIORIDADID));
            int catId = tarea.getInt(tarea.getColumnIndex(TAREA_COL_CATEGORIAID));
            int tiempoDedicado = tarea.getInt(tarea.getColumnIndex(TAREA_COL_TIEMPODEDICADO));
            int tiempoEstimado = tarea.getInt(tarea.getColumnIndex(TAREA_COL_TIEMPOESTIMADO));
            String fechaLimite = tarea.getString(tarea.getColumnIndex(TAREA_COL_FECHALIMITE));
            try {
                this.modificarTarea(idTarea, nombre, descripcion, status, catId, idPrio, tiempoDedicado, tiempoEstimado, fechaLimite);
            }catch(Exception e){
                e.printStackTrace();
            }
        }
    }

    /** Inserta una nueva categoria.
     * @param uuid El identificador de la categoria.
     * @param nombre El nombre de la categoria.
     * @param descripcion La descripcion de la categoria.
     * @param abreviatura Abreviatura de la categoria.
     * @return true si se pudo modificar, false en otro caso.
     */
    public boolean modificarCategoria(int uuid, String nombre, String abreviatura, String descripcion) throws Exception
    {
        Cursor cursor = null;
        boolean toret = false;
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        Boolean correcto =false;

        values.put( CATEGORIA_COL_ID, uuid );
        values.put( CATEGORIA_COL_NOMBRE, nombre );
        values.put( CATEGORIA_COL_DESCRIPCION, descripcion );
        values.put( CATEGORIA_COL_ABREVIATURA, abreviatura );

        try {
            db.beginTransaction();
            db.update(TABLA_CATEGORIA, values, CATEGORIA_COL_ID + "= ?", new String[]{Integer.toString(uuid)});
            db.setTransactionSuccessful();
            toret = true;
        } catch(SQLException exc)
        {
            Log.e( "DBManager.modificarCategoria", exc.getMessage() );
        }
        finally {
            if ( cursor != null ) {
                cursor.close();
            }
            db.endTransaction();
        }

        return toret;
    }


    /** Elimina una tarea de la base de datos
     * @param id El identificador de la tarea.
     * @return true si se pudo eliminar, false en otro caso.
     */
    public boolean eliminarTarea(int id)
    {
        boolean toret = false;
        SQLiteDatabase db = this.getWritableDatabase();

        try {
            db.beginTransaction();
            db.delete( TABLA_TAREA, TAREA_COL_ID + "=?", new String[]{ Integer.toString(id) } );
            db.setTransactionSuccessful();
            toret = true;
        } catch(SQLException exc) {
            Log.e( "DBManager.eliminaTarea", exc.getMessage() );
        } finally {
            db.endTransaction();
        }

        return toret;
    }

    /** Elimina una categoría de la base de datos
     * @param id El identificador de la categoría.
     * @return true si se pudo eliminar, false en otro caso.
     */
    public boolean eliminarCategoria(int id)
    {
        boolean toret = false;
        SQLiteDatabase db = this.getWritableDatabase();

        try {
            db.beginTransaction();
            db.delete( TABLA_CATEGORIA, CATEGORIA_COL_ID + "=?", new String[]{ Integer.toString(id) } );
            db.setTransactionSuccessful();
            toret = true;
        } catch(SQLException exc) {
            Log.e( "DBManager.eliminaCategoria", exc.getMessage() );
        } finally {
            db.endTransaction();
        }

        return toret;
    }

    /** Devuelve todas las tareas filtradas por categoría de la BD.
     * @param categoriaID la id de la categoría por la que filtrar.
     * @param status indica si el filtrado de tareas por estado.
     * @return Un Cursor con las tareas. */
    public Cursor getTareasCategoria(String categoriaID, int status)
    {
        if(status == -1){
            return this.getReadableDatabase().query( TABLA_TAREA,
                    null,  TAREA_COL_CATEGORIAID
                            + "= ?",
                    new String[]{categoriaID}, null, null, null );
        }else {
            return this.getReadableDatabase().query( TABLA_TAREA,
                null, TAREA_COL_CATEGORIAID + "= ? AND "+TAREA_COL_ESTADO + "= ?", new String[]{categoriaID,Integer.toString(status)}, null, null, null );
        }
    }


    /** Devuelve todas las tareas filtradas por prioridad de la BD.
     * @param prioridadID la id de la prioridad por la que filtrar.
     * @param status indica si el filtrado de tareas por estado.
     * @return Un Cursor con las tareas. */
    public Cursor getTareasPrioridad(String prioridadID, int status)
    {
        if(status == -1){
            return this.getReadableDatabase().query( TABLA_TAREA,
                    null,  TAREA_COL_PRIORIDADID
                            + "= ?",
                    new String[]{prioridadID}, null, null, null );
        }else {
            return this.getReadableDatabase().query(TABLA_TAREA,
                    null, TAREA_COL_PRIORIDADID + "= ? AND " + TAREA_COL_ESTADO + "= ?",
                    new String[]{prioridadID, Integer.toString(status)}, null, null, null);
        }
    }

    /** Devuelve todas las tareas con el estado dado.
     * @param status indica si el filtrado de tareas por estado.
     * @return Un Cursor con las tareas activas, valor de estado != 4. */
    public Cursor getTareasEstado(int status)
    {

        return this.getReadableDatabase().query( TABLA_TAREA,
                null, TAREA_COL_ESTADO + "= ?", new String[]{Integer.toString(status)},
                null, null, null );
    }

    /** Devuelve todas las tareas con el estado dado.
     * @param status indica si el filtrado de tareas por estado.
     * @return Un Cursor con las tareas activas, valor de estado != 4. */
    public Cursor getTareasFiltradas(String cat, String prio, int status)
    {
        if(status == -1){
            return this.getReadableDatabase().query( TABLA_TAREA,
                    null,  TAREA_COL_CATEGORIAID + "= ? AND " + TAREA_COL_PRIORIDADID
                            + "= ?",
                    new String[]{cat,prio}, null, null, null );
        }else{
            return this.getReadableDatabase().query( TABLA_TAREA,
                    null,  TAREA_COL_CATEGORIAID + "= ? AND " + TAREA_COL_PRIORIDADID
                            + "= ? AND "+ TAREA_COL_ESTADO + "= ?",
                    new String[]{cat,prio,Integer.toString(status)}, null, null, null );
        }

    }

    public Cursor getCatAbrv( int catId){
        return this.getReadableDatabase().query( TABLA_CATEGORIA,
                null,  CATEGORIA_COL_ID + "= ?",
                new String[]{Integer.toString(catId)}, null, null, null );
    }
    public Cursor getPrio(int prioId){
        Cursor tmp = this.getReadableDatabase().query( TABLA_PRIORIDAD,
                null,  PRIORIDAD_COL_ID+ "= ?",
                new String[]{Integer.toString(prioId)}, null, null, null );

        return tmp;
    }
    public int getPrioId(String prioName){
        Cursor tmp = this.getReadableDatabase().query( TABLA_PRIORIDAD,
                null,  PRIORIDAD_COL_NOMBRE+ "= ?",
                new String[]{prioName}, null, null, null );
        tmp.moveToFirst();
        return tmp.getInt(tmp.getColumnIndex(PRIORIDAD_COL_ID));
    }
    public int getCatId(String catName){
        Cursor tmp = this.getReadableDatabase().query( TABLA_CATEGORIA,
                null,  CATEGORIA_COL_NOMBRE+ "= ?",
                new String[]{catName}, null, null, null );
        tmp.moveToFirst();
        return tmp.getInt(tmp.getColumnIndex(CATEGORIA_COL_ID));
    }

    public void loadExampleData(SQLiteDatabase db){
        try{
            Random r = new Random();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            for(int i = 0; i < 10;i++){
                Date fecha = sdf.parse(
                        Integer.toString(2019)+"-"
                                + Integer.toString(12)+"-"
                                + Integer.toString(r.nextInt(5)+15)+" "
                                + Integer.toString( r.nextInt(23)+1)+":"
                                + Integer.toString(r.nextInt(58)+1)+":"
                                + Integer.toString( r.nextInt(58)+1));
                String fechaC = sdf.format(fecha);
                Log.d("DBFechaCreador",fechaC);
                String name = Integer.toString(i);
                this.insertarTarea(db, name,"ninguna",r.nextInt(2)+1,0,0,r.nextInt(10)+1,r.nextInt(20)+1,
                        fechaC);
            }
            this.insertarCategoria(db,"nada","ND","Aleatoria sin nada que hacer");
            this.insertarCategoria(db,"algo","ALG","Aleatoria con algunas cosas");
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    public void loadExampleData(){
        try{
            Random r = new Random();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            for(int i = 0; i < 10;i++){
                Date fecha = sdf.parse(
                        Integer.toString(r.nextInt(2019)+1)+"-"
                                + Integer.toString(r.nextInt(11)+1)+"-"
                                + Integer.toString(r.nextInt(29)+1)+" "
                                + Integer.toString( r.nextInt(23)+1)+":"
                                + Integer.toString(r.nextInt(58)+1)+":"
                                + Integer.toString( r.nextInt(58)+1));
                String fechaC = sdf.format(fecha);
                Log.d("DBFechaCreador",fecha.toString());
                String name = Integer.toString(i);
                this.insertarTarea( name,"ninguna",r.nextInt(2)+1,0,0,r.nextInt(10)+1,r.nextInt(20)+1,
                        fecha.toString());
            }
            this.insertarCategoria("nada","ND","Aleatoria sin nada que hacer");
            this.insertarCategoria("algo","ALG","Aleatoria con algunas cosas");
        }catch(Exception e){
            e.printStackTrace();
        }

    }

}
