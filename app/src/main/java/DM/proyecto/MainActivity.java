package DM.proyecto;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import DM.proyecto.DBManager.DBManager;
import DM.proyecto.SecondaryActivities.EditarCategoria;
import DM.proyecto.SecondaryActivities.EditarTarea;
import DM.proyecto.SecondaryActivities.ShowCats;
import DM.proyecto.utils.CustomCursorAdapterTareas;

public class MainActivity extends AppCompatActivity {

    private DBManager gestorDB;
    private CustomCursorAdapterTareas adaptadorDB;

    //User preferences
    private int status=-1;
    private String actualCat = null;
    private String actualPrio = null;
    private ListView listado;
    //

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        this.gestorDB = new DBManager( this.getApplicationContext());
        this.setContentView( R.layout.listado_tareas);
        ListView listado_tareas = this.findViewById( R.id.lvLista );
        this.registerForContextMenu( listado_tareas );

    }

    @Override
    public void onStart(){
        super.onStart();

        listado = this.findViewById(R.id.lvLista);
        this.adaptadorDB = new CustomCursorAdapterTareas(this,null);
        listado.setAdapter(this.adaptadorDB);
        this.registerForContextMenu(listado);
        actualizarVista();
    }

    private void actualizarVista(){

            if(actualCat == null && actualPrio == null){
                if(status > -1){
                    this.adaptadorDB.changeCursor( this.gestorDB.getTareasEstado(status) );
                }else{
                    this.adaptadorDB.changeCursor( this.gestorDB.getTareas() );
                }
            }else if (actualCat != null && actualPrio == null ){
                this.adaptadorDB.changeCursor( this.gestorDB.getTareasCategoria(actualCat,status) );
            }else if (actualCat == null && actualPrio != null){
                this.adaptadorDB.changeCursor( this.gestorDB.getTareasPrioridad(actualCat,status) );
            }else{

                this.adaptadorDB.changeCursor( this.gestorDB.getTareasFiltradas(actualCat,actualPrio,status) );
            }
            adaptadorDB.notifyDataSetChanged();
    }

    @Override
    public void onPause() {

        SharedPreferences prefs = this.getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt("status",this.status);
        editor.putString("cat",this.actualCat);
        editor.putString("prio",this.actualPrio);
        editor.apply();
        this.gestorDB.close();
        this.adaptadorDB.getCursor().close();
        super.onPause();
    }

    @Override
    protected void onResume() {
        SharedPreferences prefs = this.getPreferences(Context.MODE_PRIVATE);
        this.status = prefs.getInt("status",-1);
        this.actualPrio = prefs.getString("prio",null);
        this.actualCat = prefs.getString("cat",null);
        super.onResume();
    }

    @Override
    protected void onDestroy() {


        SharedPreferences prefs = this.getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt("status",this.status);
        editor.putString("cat",this.actualCat);
        editor.putString("prio",this.actualPrio);
        editor.apply();
        this.gestorDB.close();
        this.adaptadorDB.getCursor().close();
        super.onDestroy();
    }

    public void onCreateContextMenu(ContextMenu contxt, View v, ContextMenu.ContextMenuInfo cmi){

        this.getMenuInflater().inflate( R.menu.menu_contextual_tareas, contxt );
        contxt.setHeaderTitle( R.string.opciones );

    }

    @Override
    public boolean onContextItemSelected(MenuItem item){
        boolean aux = super.onContextItemSelected(item);
        int position = ( (AdapterView.AdapterContextMenuInfo) item.getMenuInfo()).position;
        Cursor cursor = this.adaptadorDB.getCursor();

        switch ( item.getItemId() ){
            case R.id.option_BorrarTarea:
                if ( cursor.moveToPosition( position ) ) {
                    final int id = cursor.getInt( cursor.getColumnIndex(DBManager.TAREA_COL_ID) );
                    this.gestorDB.eliminarTarea( id );
                    this.actualizarVista();
                    Toast.makeText( this, "Tarea eliminada", Toast.LENGTH_LONG ).show();
                    aux = true;
                } else {
                    String msg = "No se pudo eliminar ";
                    Log.e( "context_eliminaTarea", msg );
                    Toast.makeText( this, msg, Toast.LENGTH_LONG ).show();
                }
                break;
            case R.id.option_ChPrioridad:
                if(cursor.moveToPosition(position)){
                    final int id = cursor.getInt( cursor.getColumnIndex(DBManager.TAREA_COL_ID) );
                    showPrioridadesDialog(id);

                }else{
                    String msg = "No se pudo modificar ";
                    Log.e( "context_modificarTarea", msg );
                    Toast.makeText( this, msg, Toast.LENGTH_LONG ).show();
                }
                break;
            case R.id.option_ChCategoria:
                if(cursor.moveToPosition(position)){
                    final int id = cursor.getInt( cursor.getColumnIndex(DBManager.TAREA_COL_ID) );
                    showCategoriesDialog(id);

                }else{
                    String msg = "No se pudo modificar ";
                    Log.e( "context_modificarTarea", msg );
                    Toast.makeText( this, msg, Toast.LENGTH_LONG ).show();
                }
                break;
            case R.id.option_ChEstado:
                if(cursor.moveToPosition(position)){
                    final int id = cursor.getInt( cursor.getColumnIndex(DBManager.TAREA_COL_ID) );
                    showStatusDialog(id);

                }else{
                    String msg = "No se pudo modificar ";
                    Log.e( "context_modificarTarea", msg );
                    Toast.makeText( this, msg, Toast.LENGTH_LONG ).show();
                }
                break;
            case R.id.option_EditarTarea:
                if(cursor.moveToPosition(position)){
                    final int id = cursor.getInt( cursor.getColumnIndex(DBManager.TAREA_COL_ID) );
                    Intent myIntent = new Intent( this, EditarTarea.class );
                    myIntent.putExtra("idTarea",id);
                    this.startActivity(myIntent);
                    actualizarVista();
                }else{
                    String msg = "No se pudo modificar ";
                    Log.e( "context_modificarTarea", msg );
                    Toast.makeText( this, msg, Toast.LENGTH_LONG ).show();
                }

                break;
            case R.id.option_AddTime:
                if(cursor.moveToPosition(position)){
                    final int id = cursor.getInt( cursor.getColumnIndex(DBManager.TAREA_COL_ID) );
                    showAddTimeDialog(id);

                }else{
                    String msg = "No se pudo modificar ";
                    Log.e( "context_modificarTarea", msg );
                    Toast.makeText( this, msg, Toast.LENGTH_LONG ).show();
                }
                break;
        }

        return aux;
    }

    private void showPrioridadesDialog(final int idTarea){
        AlertDialog.Builder dlg = new AlertDialog.Builder(this);
        dlg.setTitle(R.string.prioridades);
        Cursor prioridades = gestorDB.getPrioridades();
        final String[] nombrePrio= new String[prioridades.getCount()];
        int index = 0;
        while(prioridades.moveToNext()){
            nombrePrio[index]=prioridades.getString(
                    prioridades.getColumnIndex(DBManager.PRIORIDAD_COL_NOMBRE)
            );
            index++;
        }
        dlg.setItems(nombrePrio, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int opc) {
                String prioridad = nombrePrio[opc];
                int idPrioridad = gestorDB.getPrioId(prioridad);
                gestorDB.asignarPrioridad(idTarea,idPrioridad);
                actualizarVista();
            }
        });
        dlg.create().show();
    }

    private void showCategoriesDialog(final int idTarea){
        AlertDialog.Builder dlg = new AlertDialog.Builder(this);
        dlg.setTitle(R.string.categoria);
        Cursor categorias = gestorDB.getCategorias();
        final String[] nombreCat= new String[categorias.getCount()];
        int index = 0;
        while(categorias.moveToNext()){
            nombreCat[index]=categorias.getString(
                    categorias.getColumnIndex(DBManager.CATEGORIA_COL_NOMBRE)
            );
            index++;
        }

        dlg.setItems(nombreCat, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int opc) {
                String cat = nombreCat[opc];
                int idCat = gestorDB.getCatId(cat);
                gestorDB.asignarCat(idTarea,idCat);
                actualizarVista();
            }
        });
        dlg.create().show();
    }

    private void showStatusDialog(final int idTarea){
        AlertDialog.Builder dlg = new AlertDialog.Builder(this);
        dlg.setTitle(R.string.estado);//1: Sin empezar 2: En curso 3: Pausada 4: Finalizada
        final String[] nombreEstado = {
                this.getResources().getString(R.string.sinEmpezar),
                this.getResources().getString(R.string.enCurso),
                this.getResources().getString(R.string.pausada),
                this.getResources().getString(R.string.finalizada)};

        dlg.setItems(nombreEstado, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int opc) {
                gestorDB.asignarStat(idTarea,(opc+1) );
                actualizarVista();
            }
        });
        dlg.create().show();
    }

    private void showAddTimeDialog(final int idTarea){
        AlertDialog.Builder dlg = new AlertDialog.Builder(this);
        final EditText hour = new EditText(this);
        hour.setInputType(InputType.TYPE_CLASS_NUMBER);
        dlg.setTitle(R.string.addTime);
        dlg.setMessage("Inserta tiempo a añadir en horas");
        dlg.setView(hour);
        dlg.setPositiveButton(R.string.confirmar, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                gestorDB.sumarTiempo(idTarea,Integer.parseInt(hour.getText().toString()));
            }
        });
        dlg.setNegativeButton(R.string.cancelar, null);
        dlg.create().show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        super.onCreateOptionsMenu( menu );
        this.getMenuInflater().inflate( R.menu.menu_general, menu );
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem){
        switch(menuItem.getItemId()){
            case R.id.option_addTarea:
                    Intent myIntent = new Intent( this, EditarTarea.class );
                    myIntent.putExtra("idTarea",-1);
                    this.startActivity(myIntent);
                    actualizarVista();
            case R.id.option_showallTask:
                    this.status = -1;
                    this.actualPrio = null;
                    this.actualCat = null;
                    actualizarVista();
                    break;
            case R.id.option_showNoStarted:
                    this.status = 1;
                    actualizarVista();
                    break;
            case R.id.option_showActive:
                    this.status = 2;
                    actualizarVista();
                    break;
            case R.id.option_showPaused:
                    this.status = 3;
                    actualizarVista();
                    break;
            case R.id.option_showCompleted:
                    this.status = 4;//Finalizadas
                    actualizarVista();
                    break;
            case R.id.option_verCategorias:
                    Intent showCatIntent = new Intent( this, ShowCats.class );
                    this.startActivity(showCatIntent);
                    break;
            case R.id.option_addCat:
                    Intent catIntent = new Intent( this, EditarCategoria.class );
                    catIntent.putExtra("idCat",-1);
                    this.startActivity(catIntent);
                    break;

        }

        return true;
    }


}
